import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

Vue.use(VueRouter)

var firebaseConfig = {
  apiKey: "AIzaSyDZKepAZ34jZtsEJo7gnUph25Zf7YQEbAI",
  authDomain: "e-document-20c01.firebaseapp.com",
  databaseURL: "https://e-document-20c01.firebaseio.com",
  projectId: "e-document-20c01",
  storageBucket: "e-document-20c01.appspot.com",
  messagingSenderId: "56535277475",
  appId: "1:56535277475:web:ea5bd33ba34b3f91434416",
  measurementId: "G-KKDNGJYDSP"
};

firebase.initializeApp(firebaseConfig);
export const db = firebase.firestore();
const storage = firebase.storage();
export const st = storage.ref()

Vue.mixin({
  data() {
    return {

      monthSelect: [
        { label: "ทั้งหมด", value: "0" },
        { label: "มกราคม", value: "1" },
        { label: "กุมภาพันธ์", value: "2" },
        { label: "มีนาคม", value: "3" },
        { label: "เมษายน", value: "4" },
        { label: "พฤษภาคม", value: "5" },
        { label: "มิถุนายน", value: "6" },
        { label: "กรกฎาคม", value: "7" },
        { label: "สิงหาคม", value: "8" },
        { label: "กันยายน", value: "9" },
        { label: "ตุลาคม", value: "10" },
        { label: "พฤศจิกายน", value: "11" },
        { label: "ธันวาคม", value: "12" },

      ],
      yearSelect: [
        { label: "ทั้งหมด", value: "0" },
        { label: "2020", value: "2020" },
        { label: "2021", value: "2021" },
        { label: "2022", value: "2022" },
        { label: "2023", value: "2023" },
        { label: "2024", value: "2024" },
        { label: "2025", value: "2025" },
        { label: "2026", value: "2026" },
        { label: "2027", value: "2027" },
        { label: "2028", value: "2028" },
        { label: "2029", value: "2029" },
        { label: "2030", value: "2030" },
      ],
      userMain: {
        name: "",
        surname: "",
        username: "",
        password: "",
        typeUser: "",
        userKey: ""
      },
      version: '0.0.006',
    }
  },
  methods: {
    notifyRed(messages) {
      this.$q.notify({
        color: "negative",
        position: "top",
        icon: "error",
        message: messages,
        timeout: 800
      });
    },
    notifyGreen(messages) {
      this.$q.notify({
        color: "secondary",
        position: "top",
        icon: "done",
        message: messages,
        timeout: 800
      });
    },
    loadingShow() {
      this.$q.loading.show({
        delay: 400,
        spinner: "QSpinnerHourglass"
      });
    },
    loadingHide() {
      this.$q.loading.hide();
    },
  },
})
/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  return Router
}
