
const routes = [
  {
    path: "/",
    component: () => import("pages/login.vue"),
    name: "login"
  },
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [{
      path: "/document",
      component: () => import("pages/document.vue"),
      name: "document"
    },
    {
      path: "/documentadd/",
      component: () => import("pages/documentadd.vue"),
      name: "documentadd"
    },
    {
      path: "/documentedit/:key",
      component: () => import("pages/documentadd.vue"),
      name: "documentedit"
    },
    {
      path: "/user",
      component: () => import("pages/user.vue"),
      name: "user"
    },
    {
      path: "/list",
      component: () => import("pages/list.vue"),
      name: "list"
    },

    ]
  },





]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
